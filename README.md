The challenge of the hackathon was :
>  How might Glasgow’s businesses take action to create a world leading circular city?

The **concept prototype** focuses on demonstrating desirability and shows the concept from the user point of view. <br/>
You can also find an interactive prototype for the mobile app [here](https://projects.invisionapp.com/share/JVXLE529APF?fbclid=IwAR28FsxtzXpUU9KjJYrhhyOXekJVc5L_M0Y39cZQJJoJRqolO23nrc9UEN4) .
<br/>The **data prototype** focuses on demonstrating feasibility and shows the concept from the technological point of view. 

